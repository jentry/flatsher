<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'fb_id', 'vk_id', 'vk_photo', 'fb_photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function places()
    {
        return $this->hasMany('App\Place', 'user_id');
    }

    public function vk_photo()
    {
        /*$request = 'http://api.vkontakte.ru/method/users.get?uids='.$this->vk_id.'&fields=photo_200,status';
        $response = file_get_contents($request);
        $info = array_shift(json_decode($response)->response);
        $user_photo = $info->photo_200;
        return $user_photo;*/

        return $this->vk_photo;
    }

    public function fb_photo()
    {
        //return "http://graph.facebook.com/".$this->fb_id."/picture?type=large";
        return $this->fb_photo;
    }
}
