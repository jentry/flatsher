<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Place;


class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('manager.user.index', ['users' => $users]);
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('manager.user.user', ['user' => $user]);
    }
}