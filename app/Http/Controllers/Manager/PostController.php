<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Place;


class PostController extends Controller
{
    public function index(Request $request)
    {
        if($request->type){
            if($request->type == 'hidden'){
                $places = Place::where('active', '0')->get();
            }
            elseif ($request->type == 'active'){
                $places = Place::where('active', '1')->get();
            }
        }
        else{
            $places = Place::all();
        }

        return view('manager.post.index', ['places' => $places]);
    }

    public function show($id)
    {
        $post = Place::find($id);
        return view('manager.post.show', ['post' => $post]);
    }

    public function delete(Request $request)
    {
        $post = Place::find($request->id);
        $post->delete();
        return redirect('/manager/posts')->with('mess', 'Объявление удалено');
    }

    public function create()
    {
        return view('manager.post.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'flattype' => 'required',
            'period' => 'required',
            'price' => 'required',
            'address' => 'required',
            'time' => 'required',
            'desc' => 'required'
        ]);

        $fb = $request->fb ? '1' : '0';
        $vk = $request->vk ? '1' : '0';

        //vk
        $b = json_decode(file_get_contents('https://api.vkontakte.ru/method/getProfiles?uids='.$request->vk_link.'&fields=first_name'));
        $r = $b->response[0];

        $user = User::create([
            'name' => $r->first_name,
            'email' => 'test@test.ru',
            'vk_id' => $r->uid,
            'fb_id' => $request->fb_link,
            'password' => bcrypt('testing')
        ]);

        Place::create([
            'fb' => $fb,
            'vk' => $vk,
            'flattype' => $request->flattype,
            'period' => $request->period,
            'price' => $request->price,
            'address' => $request->address,
            'time' => $request->time,
            'desc' => $request->desc,
            'active' => true,
            'user_id' => $user->id
        ]);
        return redirect('/manager/posts')->with('mess', 'Объявление добавлено');
    }

    public function change(Request $request)
    {
        $place = Place::find($request->id);
        $place->active = $request->action;
        $place->save();
        return redirect('/manager/posts')->with('mess', 'Объявление изменено');
    }
}