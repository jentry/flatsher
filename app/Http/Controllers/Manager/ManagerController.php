<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Place;


class ManagerController extends Controller
{
    public function index()
    {
        $users = User::all()->count();
        $places = Place::all()->count();
        return view('manager.index', ['users' => $users, 'places' => $places]);
    }
}