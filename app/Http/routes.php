<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Front
Route::auth();
Route::get('/',['as' => 'home', 'uses' => 'IndexController@index']);
Route::get('/home', 'HomeController@index');

Route::post('/items/load', 'IndexController@ajaxload');
Route::post('/search', 'IndexController@search');


Route::get('/add',['as'=>'add', 'uses'=>'PlaceController@index']);
Route::post('/add', 'PlaceController@store');
Route::get('/pay', 'PlaceController@pay');
Route::post('/successpay', 'PlaceController@successpay');
Route::post('/failpay', 'PlaceController@failpay');

Route::get('/boost', 'PlaceController@boost');
Route::post('/successpay', 'PlaceController@successboost');
Route::post('/failpay', 'PlaceController@failboost');

//social auth
Route::get('/fbauth', 'SocialauthController@loginWithFacebook');
Route::get('/confirmemail', 'SocialauthController@confirmemail');
Route::post('/confirm', 'SocialauthController@confirmstore');

Route::get('/vkauth', 'SocialauthController@vkauth');
Route::get('/confirmvkemail', 'SocialauthController@confirmvkemail');
Route::post('/confirmvk', 'SocialauthController@confirmstorevk');
//finish social auth

Route::put('/user/hide', 'PlaceController@hide');
Route::put('/user/repeat', 'PlaceController@repeat');

Route::post('/getvkuser', 'SocialauthController@getvkuser');
Route::post('/getfbuser', 'SocialauthController@getfbuser');

//Admin
Route::group(['prefix' => 'manager', 'middleware' => 'auth'], function()
{
    Route::get('/', 'Manager\ManagerController@index');
    Route::get('/users', 'Manager\UserController@index');
    Route::get('/users/{id}', 'Manager\UserController@show');

    Route::get('/posts', 'Manager\PostController@index');
    Route::get('/posts/add', 'Manager\PostController@create');
    Route::post('/posts/add', 'Manager\PostController@store');
    Route::put('/posts/change', 'Manager\PostController@change');
    Route::get('/posts/{id}', 'Manager\PostController@show');
    Route::delete('/posts/delete', 'Manager\PostController@delete');
});