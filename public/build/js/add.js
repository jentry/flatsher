$(document).ready(function(){
    $('#address').on('blur', function(){
       var _val = $(this).val(),
           coords;
        if(_val != ''){
            /
            $.ajax({
                type: "GET",
                url: "https://geocode-maps.yandex.ru/1.x/",
                data: {format : 'json', geocode : _val},
                success: function(data){
                    if(data.response.GeoObjectCollection.featureMember[1]) {
                        var _coords = data.response.GeoObjectCollection.featureMember[1].GeoObject.Point.pos;
                        coords = _coords.split(' ');
                        $('input[name="lat"]').val(coords[1]);
                        $('input[name="lng"]').val(coords[0]);
                    }
                    else if(data.response.GeoObjectCollection.featureMember[0]) {
                        var _coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
                        coords = _coords.split(' ');
                        $('input[name="lat"]').val(coords[1]);
                        $('input[name="lng"]').val(coords[0]);
                    }
                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }
    });

    $("#address").keyup(function(){
        var search_query = $(this).val();
        search_result = [];


        $.ajax({
            type: "GET",
            url: "https://geocode-maps.yandex.ru/1.x/",
            data: {format: 'json', geocode : search_query},
            success: function(data){
                for(var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                    search_result.push({
                        label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        value:data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        longlat:data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos});
                }

                $("#address").autocomplete({
                    source: search_result,
                    select: function(event, ui){
                        var _step = $(this).closest('.step');
                        if(!_step.hasClass('not_active')){
                            var _nextStep = _step.next('.step');
                            if(_nextStep.hasClass('not_active')){
                                _nextStep.removeClass('not_active');
                            }
                        }
                    }
                });
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });

    $.extend( $.ui.autocomplete, {
        escapeRegex: function( value ) {
            return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
        },
        filter: function(array, term) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
            return $.grep( array, function(value) {
                return matcher.test( value.label || value.value || value );
            });
        }
    });

    $.ui.autocomplete.filter = function (array, term) {
        return $.grep(array, function (value) {
            return value.label || value.value || value;
        });
    };
});