$(document).on('ready', function(){
    var _winScroll = $(window).scrollTop();
    $(window).on('scroll', function(){
       _winScroll = $(window).scrollTop();
    });

    $('#address').on('focus focusin', function(e){
        e.preventDefault();
        setTimeout(function(){
            if(!$('body').hasClass('scroll_header') && $('html').hasClass('mobile')){
                //     $('html, body').animate({scrollTop : 0}, 10);
                $(document).scrollTop(_winScroll);
            }
        }, 1000);

    });

    $('#search_form').on('submit', function(e){
        e.preventDefault();

        var self = $(this),
            _flattype = $('input[name="flattype"]:checked',self).val(),
            _address = $('input[name="address"]').val(),
            coords;


        if(_flattype != '' && _address != ''){

            $.ajax({
                type: "GET",
                url: "https://geocode-maps.yandex.ru/1.x/",
                data: {format: 'json', geocode : _address},
                beforeSend: function(){
                  $('#flats').addClass('searched');
                    $('button', self).attr('disabled', true).html('<i class="fa fa-refresh" aria-hidden="true"></i>').addClass('search');
                    //$('#search_form').addClass('loading');
                },
                success: function(data){
                    if(data.response.GeoObjectCollection.featureMember[1]){
                        var _coords = data.response.GeoObjectCollection.featureMember[1].GeoObject.Point.pos;
                        coords = _coords.split(' ');
                    }
                    else if(data.response.GeoObjectCollection.featureMember[0]){
                        var _coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
                        coords = _coords.split(' ');
                    }


                    $.ajax({
                        type: "POST",
                        url: "/search",
                        headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                        data: {flattype: _flattype, lat: coords[1], lng: coords[0]},
                        success: function(data){
                            var $grid = $('.grid').isotope({
                                itemSelector: '.grid_item',
                                masonry: {
                                    columnWidth: 280,
                                    gutter: 20
                                }
                            });
                            var $items = $(data);
                            $grid.html($items).isotope('insert', $items).isotope( 'reloadItems' );
                            $('.more_people').remove();
                            //var _top = $('#flats').offset().top;
                            //$('html, body').animate({scrollTop : _top}, 1200);
                            $('#flats').removeClass('searched');
                            //$('#search_form').removeClass('loading');
                            $('button', self).attr('disabled', false).removeClass('search').text('Выбрать жильца');
                        },
                        error: function(data){
                            console.log(data.responseText);
                        }
                    });
                },
                error: function(data){
                    console.log(data.responseText);
                }
            });
        }

    });

    $('#address').on('input paste', function(){
    //$("#address").change(function(){
        var search_query = $(this).val();
        search_result = [];

        $.ajax({
            type: "GET",
            url: "http://geocode-maps.yandex.ru/1.x/",
            data: {format : 'json', geocode: search_query},
            dataType: 'jsonp',
            success: function(data){
                for(var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                    search_result.push({
                        label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        value:data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        longlat:data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos});
                }

                $("#address").autocomplete({
                    source: search_result,
                    select: function(event, ui){
                    }
                });

            },
            error: function(data){
                console.log(data);
            }
        });
    });

    $.extend( $.ui.autocomplete, {
        escapeRegex: function( value ) {
            return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
        },
        filter: function(array, term) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
            return $.grep( array, function(value) {
                return matcher.test( value.label || value.value || value );
            });
        }
    });

    $.ui.autocomplete.filter = function (array, term) {
        return $.grep(array, function (value) {
            return value.label || value.value || value;
        });
    };
    var test = [
        {
            label : 'Россия - Москва',
            longlat : "37.620393 55.75396",
            value: 'Россия - Москва',
        },
        {
            label : 'Россия - Москва',
            longlat : "37.620393 55.75396",
            value: 'Россия - Москва',
        }
    ];
    $("#address").autocomplete({
        source: test,
        select: function(event, ui){
        }
    });

    /*$(window).load(function(){
        $.ajax({
            type: "GET",
            url: "https://geocode-maps.yandex.ru/1.x/",
            data: {format : 'json', geocode: 'москва россия', results: 5},

            beforeSend: function(){
                alert();
            },
            success: function(data){
                var search_result = [];
                for(var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                    search_result.push({
                        label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        value:data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                        longlat:data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos});
                }
                alert(search_result);

            },
            error: function(data){
                alert(data);
                console.log(data);
            }
        });
    });*/

});

