//= ../js/html5shiv.min.js
//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js
//= ../js/placeholders.min.js
//= ../../bower_components/device.js/lib/device.min.js
//= isotope.pkgd.min.js
//= ../../bower_components/jquery-form-styler/jquery.formstyler.min.js

$(document).ready(function(){
    function randomInteger(min, max) {
        var rand = min + Math.random() * (max - min)
        rand = Math.round(rand);
        return rand;
    }
    var _n = randomInteger(2,49);
    $('.now_on_site p span').text(_n);
    var _minutes = 1800000;
    setInterval(function(){
        var _n1 = randomInteger(2,49);
        $('.now_on_site p span').text(_n1);
    }, _minutes);

    $(window).on('load', function(){
        var $grid = $('.grid').isotope({
            itemSelector: '.grid_item',
            masonry: {
                columnWidth: 280,
                gutter: 20
            }
        });
        var _m = 1;
        $('a.more_people').on('click', function(e){
            e.preventDefault();
            var self = $(this),
                _offset = $('.human:not(.my)').length;
                $.ajax({
                    type: "POST",
                    url: "/items/load",
                    headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
                    data: {offset : _offset},
                    success: function(data){
                        if(data == ''){
                            self.hide();
                        }
                        else{
                            var $items = $(data);
                            $grid.append($items).isotope('appended', $items).isotope( 'reloadItems' );
                        }

                    },
                    error: function(data){
                        console.log(data.responseText);
                    }
                });
            });
    });
    var _formState = false;
    $(window).on('scroll', function(){
       var _scroll = $(window).scrollTop(),
           _heading = $('#search_people').offset().top + $('#search_people').height(),
           _c = 0;
        if(_scroll > _heading){
            $('body').addClass('scroll_header scroll_search');
            if(_c < 1){
                if(_formState){
                    $('#search_form').show();
                }
                else{
                    $('#search_form').hide();
                }
                _c++;
            }

        }
        else{
            $('body').removeClass('scroll_header scroll_search');
            $('#search_form').show();
            _c = 0;
        }

    });
    $('a.close_mess').on('click', function(e){
        e.preventDefault();
        $('.after_posting').fadeOut(200);
    });

    $('select').styler();

    $('header a.open_search').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        $('#search_form').fadeIn(200, function(){
            _formState = true;
            self.fadeOut(200, function(){
               $('header a.like_close').fadeIn(200);
            });
        });
    });
    $('header a.like_close').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        $('#search_form').fadeOut(200, function(){
            _formState = false;
            self.fadeOut(200, function(){
                $('header a.open_search').fadeIn(200);
            });
        });
    });


    $('.hide_me').on('click', function(e){
        e.preventDefault();
        var _placeId = $(this).attr('data-id'),
            _user = $(this).attr('data-user');
        $.ajax({
            type: "PUT",
            url: "/user/hide",
            headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
            data: {place : _placeId, user : _user},
            success: function(data){
                location.reload();
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });
    $('.repeat').on('click', function(e){
        e.preventDefault();
        var _placeId = $(this).attr('data-id'),
            _user = $(this).attr('data-user');
        $.ajax({
            type: "PUT",
            url: "/user/repeat",
            headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
            data: {place : _placeId, user : _user},
            success: function(data){
                location.reload();
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    });

    //
    $('input.social').on('change', function(){
       var _state = $(this).prop('checked');
        if(_state){
            $(this).next('label').hide();
            $(this).next('label').next('div').show();
        }
        else{
            $(this).next('label').show();
            $(this).next('label').next('div').hide();
        }
    });

    $(window).on('load', function(){
       $('input.social').each(function(){
           var _state = $(this).prop('checked');
           if(_state){
               $(this).next('label').hide();
               $(this).next('label').next('div').show();
           }
       });
        var _checkedProfiles = $('input.social:checked'),
            _addForm = $('#add_form');
        if(_checkedProfiles.length > 0){
            $('.step', _addForm).eq(1).removeClass('not_active');
        }
    });

    $('a.log').on('click', function(e){
       e.preventDefault();
       var _block = $('.social_wind');
       if(_block.css('display') == 'none'){
           var _right = ($(window).width() - $('.container').width())/2;
           _block.css({'right' : _right}).fadeIn(200);
       }
        else{
           _block.fadeOut(200);
       }
    });
    $(document).on('click', function(e){
        var _block = $('.social_wind'),
            _a = $('a.log');
        if( $(e.target).closest('a.log').length === 0 && $(e.target).closest(_block).length === 0){
            _block.fadeOut(200);
        }
    });

    $(document).on('click', '.links.not_active a', function(e){
       e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });
    $(document).on('click', '.human.out_radius a', function(e){
        e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });
    $(document).on('click', '.human.not_searched a', function(e){
        e.preventDefault();
        var _human = $(this).closest('.human');
        $('.already', _human).removeClass('hide');
        setTimeout(function(){
            $('.already', _human).addClass('hide');
        }, 3000);
    });

});