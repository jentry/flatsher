$(document).ready(function(){
   $('#vk_link').on('change', function(){
       var _link = $(this).val();
       if(_link != ''){
           getVKUser(_link);
       }
   });

    $('#fb_link').on('change', function(){
        var _link = $(this).val();
        if(_link != ''){
            getFbUser(_link);
        }
    });

   function getVKUser(link){
       $.ajax({
           type: "POST",
           url: "/getvkuser",
           headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
           data: {user : link},
           success: function(data){
               
               if(data.stat == 1){
                   Materialize.toast(data.res, 4000);
               }
           },
           error: function(data){
               console.log(data.responseText);
           }
       });
   }

    function getFbUser(link){
        $.ajax({
            type: "POST",
            url: "/getfbuser",
            headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr("content")},
            data: {user : link},
            success: function(data){
                console.log(data);
                if(data.stat == 1){
                    Materialize.toast(data.res, 4000);
                }
            },
            error: function(data){
                console.log(data.responseText);
            }
        });
    }
});