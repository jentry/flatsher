@extends('layouts.layout')

@section('title') Стать жильцом @endsection

@section('content')

    <div class="add_page">
        <div class="container group">
            <div class="heading group relative">
                <h1>Стать жильцом</h1>
                <div class="return"><a href="{{url('/')}}"></a>
                    <p>Лучше 100 лет буду искать</p>
                </div>
            </div>
        </div>
        {!! Form::open(['url' => '/add', 'method' => 'POST', 'id' => 'add_form']) !!}
            <div class="step">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 1.</p>
                        <p>Выберите профиль для контакта <span>(можете оба, чаты всегда удобнее звонков)</span>
                        </p>
                    </div>
                    @if(!Auth::guest())
                    <div class="value_block group">
                        @if(Auth::user()->vk_id)
                            <input type="checkbox" name="vk" value="1" id="profile1" class="social" <?php if($authtype == 'vk'){ echo 'checked'; }?>>
                            <label for="profile1" class="vk_label">Вконтакте</label>
                            <div style="float:left;display: none;margin-right:10px;">
                                <img src="{{Auth::user()->vk_photo()}}" alt="" class="img-circle" style="width:50px;">
                                <label for="profile1">Убрать</label>
                            </div>
                        @else
                            <a href="/vkauth?redurl=/add" class="vk_label">Войти <i class="fa fa-vk"></i> </a>
                        @endif
                        @if(Auth::user()->fb_id)
                            <input type="checkbox" name="fb" value="1" id="profile2" class="social" <?php if($authtype == 'fb'){ echo 'checked'; }?>>
                            <label for="profile2" class="fb_label">Facebook</label>
                                <div style="float:left;display: none;margin-right:10px;">
                                    <img src="{{Auth::user()->fb_photo()}}" alt="" class="img-circle" style="width:50px;">
                                    <label for="profile2">Убрать</label>
                                </div>
                        @else
                                <a href="/fbauth?redurl=/add" class="fb_label">Войти <i class="fa fa-facebook"></i></a>
                        @endif
                    </div>
                    @else
                        <div class="value_block group">
                            <a href="/vkauth?redurl=/add" class="vk_label">Войти <i class="fa fa-vk"></i> </a>
                            <a href="/fbauth?redurl=/add" class="fb_label"> Войти <i class="fa fa-facebook"></i></a>
                        </div>
                    @endif

                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 2.</p>
                        <p>Вы ищите</p>
                    </div>
                    <div class="value_block group">
                        <input type="radio" name="type" value="1" id="flat1" class="flats">
                        <label for="flat1" class="room_type type1">Комнату</label>
                        <input type="radio" name="type" value="2" id="flat2" class="flats">
                        <label for="flat2" class="room_type type2">Квартиру</label>
                    </div>
                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 3.</p>
                        <p>На период в</p>
                    </div>
                    <div class="value_block group">
                        <select name="period">
                            @for($i = 1;$i < 13; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select> месяц
                    </div>
                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 4.</p>
                        <p>С ежемесячной платой <span>(включая все платежи)</span>
                        </p>
                    </div>
                    <div class="value_block group">
                        <input type="text" name="price" value=""> рублей
                    </div>
                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 5.</p>
                        <p>Укажите адрес, который вы чаще всего будете посещать <span>(место работы, садик ребёнка, любимый парк, родственники)</span>
                        </p>
                    </div>
                    <div class="value_block group">
                        <input type="text" name="address" value="" id="address">
                        <input type="hidden" name="lat" value="">
                        <input type="hidden" name="lng" value="">
                    </div>
                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 6.</p>
                        <p>Время в пути <span>(в каком радиусе от этого адреса вы готовы жить)</span>
                        </p>
                    </div>
                    <div class="value_block group">
                        <select name="time">
                            @for($n = 5;$n<91;$n=$n+5)
                            <option value="{{$n}}">{{$n}}</option>
                            @endfor

                        </select> минут
                    </div>
                </div>
            </div>
            <div class="step not_active">
                <div class="container group">
                    <div class="title_block">
                        <p class="title">Шаг 7.</p>
                        <p>Ваши пожелания <span>(что там должно быть для вас)</span>
                        </p>
                    </div>
                    <div class="value_block group">
                        <textarea name="comment"></textarea>
                    </div>
                </div>
            </div>
            <div class="step">
                <div class="container">
                    <p class="mess">Вы можете разместить несколько объявлений с разной локацией и условиями</p>
                </div>
            </div>
            <button type="submit">Разместить объявление</button>
        {!! Form::close() !!}
    </div>

@endsection