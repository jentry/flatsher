@extends('layouts.layout')

@section('title') Поторопить собственников @endsection

@section('content')

    <div class="add_page boost">
        <div class="container group">
            <div class="heading group relative">
                <h1>Страница<br>не найдена</h1>
                <div class="return"><a href="{{url('/')}}"></a>
                    <p>Лучше 100 лет буду искать</p>
                </div>
            </div>
        </div>
        <div id="pay_block">
            <div class="container group">
                <div class="text">
                    <p class="bold text-center">Если вопрос срочный, то вы можете ускорить поиск в 2 раза!</p>
                    <div style="margin-top:55px;" class="summ">
                        <p class="title">Сумма к оплате:</p><span>{{$summ}} ₽</span>
                    </div>
                    <p>На эту сумму мы запустим рекламу в Facebook и Instagram, чтобы привлечь подходящих вам собственников.</p>
                </div>
            </div>
        </div>
        @if(Auth::guest())
            <p class="text-center">Вы не авторизированы и не можете ускорить поиск</p>
        @else
        <form method="post" action="https://wl.walletone.com/checkout/checkout/Index" id="pay_form">
            <input name="WMI_MERCHANT_ID" type="hidden"    value="127120557232"/>
            <input name="WMI_PAYMENT_AMOUNT" type="hidden" value="1.00"/>
            <input name="WMI_CURRENCY_ID" type="hidden"    value="643"/>
            <input name="WMI_DESCRIPTION" type="hidden"    value="Ускорение объявления пользователем {{Auth::user()->id}}"/>
            <input name="WMI_SUCCESS_URL" type="hidden"    value="{{url('/successboost')}}"/>
            <input name="WMI_FAIL_URL" type="hidden"       value="{{url('/failboost')}}"/>
            <button type="submit">Оплатить</button>
        </form>
            @endif
    </div>

@endsection