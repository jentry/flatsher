@extends('layouts.layout')

@section('title') Оплата обьявления @endsection

@section('content')

    <div class="add_page">
        <div class="container group">
            <div class="heading group relative">
                <h1>Оплата</h1>
                <div class="return"><a href="#"></a>
                    <p>Лучше 100 лет буду искать</p>
                </div>
            </div>
        </div>
        <div id="pay_block">
            <div class="container group">
                <div class="text">
                    <p class="bold">Ваше объявление размещается на 90 дней, в любой момент его можно скрыть, а также возобновлять поиск неограниченное число раз.</p>
                    <p>Вы можете разместить несколько объявлений с разной локацией и условиями.</p>
                    <div class="summ">
                        <p class="title">Сумма к оплате:</p><span>{{$summ}} ₽</span>
                    </div>
                    <p>На эту сумму мы запустим рекламу в Facebook и Instagram, чтобы привлечь подходящих вам собственников.</p>
                </div>
            </div>
        </div>
        <form method="post" action="https://wl.walletone.com/checkout/checkout/Index" id="pay_form">
            <input name="WMI_MERCHANT_ID" type="hidden"    value="127120557232"/>
            <input name="WMI_PAYMENT_AMOUNT" type="hidden" value="1.00"/>
            <input name="WMI_CURRENCY_ID" type="hidden"    value="643"/>
            <input name="WMI_DESCRIPTION" type="hidden"    value="Оплата объявления #{{$id}} пользователем"/>
            <input name="WMI_SUCCESS_URL" type="hidden"    value="{{url('/successpay')}}"/>
            <input name="WMI_FAIL_URL" type="hidden"       value="{{url('/failpay')}}"/>
            <input type="hidden" name="PLACEID" value="">
            <button type="submit">Оплатить и стать жильцом</button>
        </form>
    </div>

@endsection