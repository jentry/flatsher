@extends('layouts.layout')
@section('title') Ваш email @endsection
@section('content')
<div class="add_page">
    <div class="container group">
        <div class="heading group relative">
            <h1>Ваш email</h1>
        </div>
    </div>
    {!! Form::open(['url' => '/confirm', 'method' => 'POST', 'id' => 'add_form']) !!}
    <div class="step">
        <div class="container group">
            <div class="title_block">
                <p class="title">Ваш email:</p>
                </p>
            </div>
            <div class="value_block group" style="margin-top:0;">
                {!! Form::email('email', '', ['style' => 'width:260px;']) !!}
                {!! Form::hidden('url', $url) !!}
            </div>
        </div>
    </div>

    <button type="submit">Завершить вход</button>
    {!! Form::close() !!}
</div>
@endsection