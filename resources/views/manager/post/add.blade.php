@extends('layouts.manager')

@section('content')
    <section>
        <div class="container">
            <h3 >Добавить объявление</h3>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(['/manager/posts/add', 'method' => 'POST']) !!}
                <div class="row">
                    <div class="col s12">
                        <div class="row">

                            <div class="input-field col s12">
                                <input id="vk_link" type="text" class="validate" name="vk_link">
                                <label for="vk_link">Ссылка на профиль vk</label>
                            </div>
                            <div class="input-field col s12">
                                <input id="fb_link" type="text" class="validate" name="fb_link">
                                <label for="fb_link">Ссылка на профиль fb</label>
                            </div>

                            <div class="input-field col s12">
                                <input id="price" type="text" class="validate" name="price">
                                <label for="price">Оплата</label>
                            </div>
                            <div class="input-field col s12">
                                <input id="period" type="text" class="validate" name="period">
                                <label for="period">Период(мес)</label>
                            </div>

                            <div class="input-field col s12">
                                <textarea name="desc" id="desc" class="materialize-textarea"> </textarea>
                                <label for="desc">Описание</label>
                            </div>
                            <div class="input-field col s12">
                                <p>
                                    <input name="flattype" type="radio" id="type1" value="1">
                                    <label for="type1">Комната</label>
                                </p>
                                <p>
                                    <input name="flattype" type="radio" id="type2" value="2">
                                    <label for="type2">Квартира</label>
                                </p>
                            </div>
                            <div class="input-field col s12">
                                <input name="address" id="address" type="text" name="address">
                                <label for="address">Адрес, который вы будете посещать</label>
                            </div>
                            <div class="input-field col s12">
                                <input name="time" id="time" type="text">
                                <label for="time">Время на дорогу</label>
                            </div>
                            <div class="input-field col s12">
                                <p>
                                    <input type="checkbox" id="soc1" value="1" name="vk" />
                                    <label for="soc1">Показать ссылку на vk в объявлении</label>
                                </p>
                                <p>
                                    <input type="checkbox" id="soc2" value="2" name="fb" />
                                    <label for="soc2">Показать ссылку на facebook в объявлении</label>
                                </p>
                            </div>
                            <button type="submit" class="btn" style="margin-top: 30px;">Опубликовать</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>


@endsection