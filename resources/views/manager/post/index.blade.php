@extends('layouts.manager')

@section('content')
    <section>
        <div class="container">
            <h1>Объявления</h1>
            @if(session('mess'))
                <p>{{session('mess')}}</p>
            @endif
            <div>
                <a href="/manager/posts" class="waves-effect waves-light btn">Все</a>
                <a href="/manager/posts?type=hidden" class="waves-effect waves-light btn">Скрытые <i class="material-icons right">visibility_off</i></a>
                <a href="/manager/posts?type=active" class="waves-effect waves-light btn">Активные <i class="material-icons right">visibility</i> </a>
            </div>
            <div style="margin:10px 0;">
                <a href="/manager/posts/add" class="waves-effect waves-light btn">Добавить объявление <i class="material-icons right">note_off</i></a>
            </div>
            <div class="collection">
                @foreach($places as $post)
                    <a href="{{url('/manager/posts/'.$post->id)}}" class="collection-item">{{$post->user->name}}</a>
                @endforeach
            </div>
        </div>
    </section>


@endsection