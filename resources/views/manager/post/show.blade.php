@extends('layouts.manager')

@section('content')
    <section>
        <div class="container">
            <h1>{{$post->user->name}}</h1>
            <div style="width:150px;">
            <div class="card">
                <div class="card-image">
                    @if($post->user->vk_id)
                        <img src="{{$post->user->vk_photo()}}" alt="" style="width:150px;">
                    @elseif($post->user->fb_id)
                        <img src="{{$post->user->fb_photo()}}" alt="" style="width:150px;">
                    @endif
                </div>
            </div>
            </div>
            <p>Оплата: {{$post->price}} руб.</p>
            <p>Период: {{$post->period}} мес.</p>
            <p>Описание: {{$post->desc}}</p>
            <?php
$types = ['', 'Комната', 'Квартира'];
            ?>
            <p>Тип помещения: {{$types[$post->flattype]}}</p>
            <p>Адрес: {{$post->address}}</p>
            <p>Время в пути: {{$post->time}} минут</p>
            {!! Form::open(['url' => '/manager/posts/delete', 'method' => 'DELETE']) !!}
            {!! Form::hidden('id', $post->id) !!}
            <button type="submit" class="btn waves-effect waves-light red darken-3" style="margin-bottom: 20px;">Удалить</button>
            {!! Form::close() !!}

            {!! Form::open(['url' => '/manager/posts/change', 'method' => 'PUT']) !!}
            {!! Form::hidden('id', $post->id) !!}
            @if($post->active)
            {!! Form::hidden('action', 0) !!}
                <button type="submit" class="btn waves-effect waves-light">Скрыть</button>
            @else
            {!! Form::hidden('action', 1) !!}
                <button type="submit" class="btn waves-effect waves-light">Открыть</button>
            @endif

            {!! Form::close() !!}
        </div>
    </section>


@endsection