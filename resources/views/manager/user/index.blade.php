@extends('layouts.manager')

@section('content')
    <section>
        <div class="container">
            <div class="collection">
                @foreach($users as $user)
                    <a href="{{url('/manager/users/'.$user->id)}}" class="collection-item">{{$user->name}}</a>
                @endforeach
            </div>
        </div>
    </section>


@endsection