@extends('layouts.manager')

@section('content')
    <section>
        <div class="container">
            <h3>{{$user->name}}</h3>
            <p>Профиль в <a href="http://vk.com/id{{$user->vk_id}}" target="_blank">Вконтакте</a></p>
            <p>Профиль в <a href="http://facebook.com/{{$user->fb_id}}" target="_blank">Facebook</a></p>
            <p>Email: {{$user->email}}</p>
        </div>
    </section>


@endsection