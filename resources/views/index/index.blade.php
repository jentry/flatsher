@extends('layouts.layout')

@section('title') Главная @endsection

@section('content')
    <div id="search_people">
        <h1>Сервис надёжных<br>жильцов</h1>
        <h2>Аренда квартир и комнат без риелторов</h2>
        <div class="container">
            <form id="search_form" class="group">
                <div class="types group">
                    <input type="radio" name="flattype" value="1" id="type1" checked>
                    <label for="type1">Квартира</label>
                    <input type="radio" name="flattype" value="2" id="type2">
                    <label for="type2">Комната</label>
                </div>
                <div class="search group">
                    <input type="text" name="address" placeholder="По адресу" id="address">
                    <button type="submit">Выбрать жильца</button>
                </div>
            </form>
        </div>
    </div>
    @if (session('mess1'))
    <div class="after_posting">
        <div class="container relative">
            <p>{{session('mess1')}}</p>
            <p>{{session('mess2')}}</p>
            <a href="#" class="close_mess"></a>
        </div>
    </div>
    @endif
    <div id="flats">
        <div class="container group">
            <div class="grid">
                <div class="now_on_site grid_item">
                    <div class="like_table">
                        <div class="table_cell">
                            <p>Сейчас <span>29</span>собственников выбирают жилье
                            </p>
                        </div>
                    </div>
                </div>
                @if(!Auth::guest())
                    @foreach($my as $place)
                <div class="human grid_item my">
                    <div class="foto">
                        @if($place->user->vk_id)
                            <img src="{{$place->user->vk_photo()}}" alt="" class="img-responsive">
                        @elseif($place->user->fb_id)
                            <img src="{{$place->user->fb_photo()}}" alt="" class="img-responsive">
                        @endif
                            <div class="your_block">Ваше объявление</div>
                    </div>
                    <div class="money group">
                        <div class="pay">
                            <p class="title">Оплата</p>
                            <p class="value">{{$place->price}} ₽</p>
                        </div>
                        <div class="time">
                            <p class="title">Период</p>
                            <p class="value">{{$place->period}} мес.</p>
                        </div>
                    </div>
                    <div class="desc">
                        <p>
                            {{$place->desc}}
                        </p>
                    </div>
                    @if($place->active)
                    <a href="#" class="hide_me" data-id="{{$place->id}}" data-user="{{Auth::user()->id}}">Уже заселился,<br>спрячьте меня</a>
                    @else
                    <div class="repeat_links">
                        <a href="#" class="repeat" data-id="{{$place->id}}" data-user="{{Auth::user()->id}}">Возобновить поиск</a>
                        <a href="{{url('/add')}}" class="new">Новое объявление</a>
                    </div>
                    @endif
                    <div class="links group">
                    @if($place->vk && $place->fb)
                            <a href="http://vk.com/im?sel={{$place->user->vk_id}}" class="half vk" target="_blank"><i class="fa fa-vk"></i></a>
                            <a href="https://www.facebook.com/messages/{{$place->user->fb_id}}" class="half fb" target="_blank"><i class="fa fa-facebook"></i></a>
                    @else
                        @if($place->vk)
                                <a href="http://vk.com/im?sel={{$place->user->vk_id}}" target="_blank" class="full vk"><i class="fa fa-vk"></i></a>
                        @elseif($place->fb)
                                <a href="https://www.facebook.com/messages/{{$place->user->fb_id}}" target="_blank" class="full fb"><i class="fa fa-facebook"></i></a>
                        @endif
                    @endif

                    </div>
                </div>
                    @endforeach
                @endif
                @foreach($places as $place)
                <div class="human grid_item not_searched">
                    <div class="foto">
                        @if($place->user->vk_id)
                            <img src="{{$place->user->vk_photo()}}" alt="" class="img-responsive">
                        @elseif($place->user->fb_id)
                            <img src="{{$place->user->fb_photo()}}" alt="" class="img-responsive">
                        @endif

                        <div class="already hide">
                            <div class="like_table">
                                <div class="table_cell">
                                    <p>Сначала настройте поиск <br>:(</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="money group">
                        <div class="pay">
                            <p class="title">Оплата</p>
                            <p class="value">{{$place->price}} ₽</p>
                        </div>
                        <div class="time">
                            <p class="title">Период</p>
                            <p class="value">{{$place->period}} мес.</p>
                        </div>
                    </div>
                    <div class="desc">
                        <p>{{$place->desc}}</p>
                    </div>
                    <div class="links group <?php if(!$place->active){ echo 'not_active'; }; ?>">
                        @if($place->vk && $place->fb)
                            <a href="http://vk.com/im?sel={{$place->user->vk_id}}" class="half vk" target="_blank"><i class="fa fa-vk"></i></a>
                            <a href="https://www.facebook.com/messages/{{$place->user->fb_id}}" class="half fb" target="_blank"><i class="fa fa-facebook"></i></a>
                        @else
                            @if($place->vk)
                                <a href="http://vk.com/im?sel={{$place->user->vk_id}}" target="_blank" class="full vk"><i class="fa fa-vk"></i></a>
                            @elseif($place->fb)
                                <a href="https://www.facebook.com/messages/{{$place->user->fb_id}}" target="_blank" class="full fb"><i class="fa fa-facebook"></i></a>
                            @endif
                        @endif

                    </div>
                </div>
                @endforeach

            </div><a href="#" class="more_people"></a>
        </div>
    </div>
    <div id="alert1" class="modal fade alertmodal">
        <div class="like_table">
            <div class="table_cell">
                <div class="modal_content">
                    <h3>Сначала настройте поиск</h3>
                </div>
            </div>
        </div>
    </div>
    <div id="alert2" class="modal fade alertmodal">
        <div class="like_table">
            <div class="table_cell">
                <div class="modal_content">
                    <h3>Ищет в другом месте.</h3>
                    <p>Жилец не согласится.</p>
                </div>
            </div>
        </div>
    </div>
@endsection