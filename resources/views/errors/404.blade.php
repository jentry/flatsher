<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="token" content="{{  csrf_token() }}">
    <link href="{{asset('build/css/screen.css')}}" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <title>Страница не найдена</title>
</head>
<body>
<header>
    <div class="container group relative"><a href="{{url('/')}}" id="logo"></a>

    </div>
</header>

<div class="add_page boost">
    <div class="container group">
        <div class="heading group relative">
            <h1>Страница<br>не найдена</h1>
        </div>
    </div>
    <div id="pay_block">
        <div class="container group">
            <div class="text">
                <p class="bold text-center">Перейдите на <a href="{{url('/')}}">главную</a> для продолжения работы</p>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{asset('build/js/functions.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="https://api-maps.yandex.ru/1.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('build/js/steps.js')}}"></script>
@if(Route::currentRouteName() == 'home')
    <script type="text/javascript" src="{{asset('build/js/search.js')}}"></script>
@endif
@if(Route::currentRouteName() == 'add')
    <script type="text/javascript" src="{{asset('build/js/add.js')}}"></script>
@endif
</body>
</html>