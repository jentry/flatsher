<?php
use App\User;
use App\Place;
        ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="token" content="{{  csrf_token() }}">
    <link href="{{asset('build/css/screen.css')}}" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('build/css/jqueryui.css')}}">
    <title>@yield('title')</title>
</head>
<body>
<header>
    <div class="container group"><a href="{{url('/')}}" id="logo"></a>
        @if (Auth::guest())
        <div class="login group">
            <a href="{{url('/add')}}" class="to_be">Стать жильцом</a>
            <a href="#" class="log">Войти</a>
            <a href="#" class="like_close"></a>
            <a href="#" class="open_search"></a>
        </div>
        <div class="social_wind">
            <p><a href="{{url('/vkauth')}}?redurl=/" class="half"><i class="fa fa-vk"></i></a> <a href="{{url('/fbauth')}}?redurl=/" class="half"><i class="fa fa-facebook"></i></a></p>
        </div>
        @else
<?php
$user = User::find(Auth::user()->id);
$activePlaces = Place::where([
    ['active', 1],
    ['user_id', $user->id]
])->get();
            ?>
        <div class="logged">
            @if(count($user->places) == 0 or count($activePlaces) == 0)
            <a href="{{url('/add')}}" class="to_be">Стать жильцом</a>
            @else
            <a href="{{url('/boost')}}" class="speed">Поторопить собственников</a>
            @endif
            <p class="user"><span>Привет, {{Auth::user()->name}}</span><a href="{{url('/logout')}}" class="logout">Выйти</a></p>
                <a href="#" class="like_close"></a>
                <a href="#" class="open_search"></a>
        </div>
        @endif
    </div>
</header>

@yield('content')

<script type="text/javascript" src="{{asset('build/js/functions.js')}}"></script>
<script src="{{asset('build/js/jqueryui.js')}}"></script>
<script src="https://api-maps.yandex.ru/1.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('build/js/steps.js')}}"></script>
@if(Route::currentRouteName() == 'home')
<script type="text/javascript" src="{{asset('build/js/search.js')}}"></script>
@endif
@if(Route::currentRouteName() == 'add')
    <script type="text/javascript" src="{{asset('build/js/add.js')}}"></script>
@endif
</body>
</html>