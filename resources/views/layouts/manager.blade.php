<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="token" content="{{  csrf_token() }}">
    <link href="{{asset('managerstyles/css/materialize.css')}}" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <title>Админпанель</title>
</head>
<body>
<header>
    <div class="container group">
        <nav class="teal lighten-2">
            <div class="nav-wrapper">
                <a href="{{url('/manager')}}" class="brand-logo">Админка</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="{{url('/manager/users')}}">Пользователи</a></li>
                    <li><a href="{{url('/manager/posts')}}">Объявления</a></li>
                    <li><a href="{{url('/logout')}}">Выйти</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>

@yield('content')

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="{{asset('managerstyles/js/materialize.js')}}"></script>
<script type="text/javascript" src="{{asset('managerstyles/js/manager.js')}}"></script>
</body>
</html>