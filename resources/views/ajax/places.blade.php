<?php
use App\User;
?>
<div class="now_on_site grid_item">
    <div class="like_table">
        <div class="table_cell">
            <p>Сейчас <span><?= rand(2,49); ?></span>собственников выбирают жилье
            </p>
        </div>
    </div>
</div>
@foreach($places[0] as $place)
    <?php
$user = User::find($place->user_id);
    ?>
    <div class="human grid_item">
        <div class="foto">
            @if($user->vk_id)
                <img src="{{$user->vk_photo()}}" alt="" class="img-responsive">
            @elseif($user->fb_id)
                <img src="{{$user->fb_photo()}}" alt="" class="img-responsive">
            @endif
        @if(!$place->active)
                <div class="already hide">
                    <div class="like_table">
                        <div class="table_cell">
                            <p>Уже стал жильцом,<br>вы чуть-чуть не успели<br>:(</p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="money group">
            <div class="pay">
                <p class="title">Оплата</p>
                <p class="value">{{$place->price}} ₽</p>
            </div>
            <div class="time">
                <p class="title">Период</p>
                <p class="value">{{$place->period}} мес.</p>
            </div>
        </div>
        <div class="desc">
            <p>{{$place->desc}}</p>
        </div>
        <div class="links group <?php if(!$place->active){ echo 'not_active'; }; ?>">
            @if($place->vk && $place->fb)
                <a href="http://vk.com/im?sel={{$user->vk_id}}" class="half vk" target="_blank"><i class="fa fa-vk"></i></a>
                <a href="https://www.facebook.com/messages/{{$user->fb_id}}" class="half fb" target="_blank"><i class="fa fa-facebook"></i></a>
            @else
                @if($place->vk)
                    <a href="http://vk.com/im?sel={{$user->vk_id}}" target="_blank" class="full vk"><i class="fa fa-vk"></i></a>
                @elseif($place->fb)
                    <a href="https://www.facebook.com/messages/{{$user->fb_id}}" target="_blank" class="full fb"><i class="fa fa-facebook"></i></a>
                @endif
            @endif

        </div>
    </div>
@endforeach

@foreach($places[1] as $place)
    <?php
    $user = User::find($place->user_id);
    ?>
    <div class="human grid_item out_radius">
        <div class="foto">
            @if($user->vk_id)
                <img src="{{$user->vk_photo()}}" alt="" class="img-responsive">
            @elseif($user->fb_id)
                <img src="{{$user->fb_photo()}}" alt="" class="img-responsive">
            @endif
                <div class="already hide">
                    <div class="like_table">
                        <div class="table_cell">
                            <p>ищет в другом месте<br>жилец не согласится<br>:(</p>
                        </div>
                    </div>
                </div>
        </div>
        <div class="money group">
            <div class="pay">
                <p class="title">Оплата</p>
                <p class="value">{{$place->price}} ₽</p>
            </div>
            <div class="time">
                <p class="title">Период</p>
                <p class="value">{{$place->period}} мес.</p>
            </div>
        </div>
        <div class="desc">
            <p>{{$place->desc}}</p>
        </div>
        <div class="links group <?php if(!$place->active){ echo 'not_active'; }; ?>">
            @if($place->vk && $place->fb)
                <a href="http://vk.com/im?sel={{$user->vk_id}}" class="half vk" target="_blank"><i class="fa fa-vk"></i></a>
                <a href="https://www.facebook.com/messages/{{$user->fb_id}}" class="half fb" target="_blank"><i class="fa fa-facebook"></i></a>
            @else
                @if($place->vk)
                    <a href="http://vk.com/im?sel={{$user->vk_id}}" target="_blank" class="full vk"><i class="fa fa-vk"></i></a>
                @elseif($place->fb)
                    <a href="https://www.facebook.com/messages/{{$user->fb_id}}" target="_blank" class="full fb"><i class="fa fa-facebook"></i></a>
                @endif
            @endif

        </div>
    </div>
@endforeach